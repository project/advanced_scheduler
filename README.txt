CONTENTS OF THIS FILE
----------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
 This module provide us to publish workbench integrated content with scheduler.
 You need to just install these modules and create moderation states through
 workbench moderation module. This module provides you a new tab (Scheduled
 states) in scheduler. Then scheduler would publish only these configured
 states on  scheduled dates and time after cron job execution.
 
 
 * For a full description of the module visit:
   https://www.drupal.org/project/advanced_schdeuler

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/advanced_schdeuler

REQUIREMENTS
------------
*Scheduler
*Workbench moderation

INSTALLATION
--------------
 * Install the Redirect after login module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.

CONFIGURATION
--------------
  None

MAINTAINERS
------------

 * Piyush Rai (Piyush_Rai) - https://www.drupal.org/u/Piyush_Rai
 * Shamsher Alam (Shamsher_Alam) - https://www.drupal.org/u/Shamsher_Alam

Supporting organization:

 * Sapient - https://www.drupal.org/sapient

Sapient is a marketing and consulting company that provides business, marketing,
and technology services to clients.
